package com.epam.test;

import com.epam.framework.ui.Browser;
import com.epam.framework.util.Randoms;
import com.epam.product.mailRu.account.businessModel.Account;
import com.epam.product.mailRu.account.businessModel.AccountFactory;
import com.epam.product.mailRu.account.service.CloudLoginService;
import com.epam.product.mailRu.cloud.exception.CloudException;
import com.epam.product.mailRu.cloud.service.CloudService;
import org.testng.annotations.*;

public class CloudTest {

    private static final String FILE_NAME = "file.txt";

    private CloudLoginService cloudLoginService = new CloudLoginService();
    private CloudService cloudService = new CloudService();

    private Account account = AccountFactory.getExcistanceAccount();

    @BeforeMethod
    public void login() {
        cloudLoginService.login(account);
    }


    @Test
    public void createFolder() {
        cloudService.findCreatedFolder(Randoms.randomAlphanumeric());
    }

    @Test(expectedExceptions = CloudException.class, expectedExceptionsMessageRegExp = "file did not found \\w*")
    public void deleteFolder() {
        cloudService.findRemovedFolder(Randoms.randomAlphanumeric());
    }

    @Test
    public void uploadFile() {
        cloudService.uploadFile(FILE_NAME);
    }

    @Test
    public void dragAndDrop() {
        cloudService.moveFromTo(FILE_NAME,Randoms.randomAlphanumeric());
    }

    @Test
    public void shareFile() {
        cloudService.uploadAndShareFile(FILE_NAME);
    }

    @AfterMethod
    public void stopDriver() {
        Browser.getBrowser().quit();
    }

}
