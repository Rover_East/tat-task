package com.epam.test;

import com.epam.framework.ui.Browser;
import com.epam.product.mailRu.account.businessModel.Account;
import com.epam.product.mailRu.account.businessModel.AccountFactory;
import com.epam.product.mailRu.account.exception.AuthenticationException;
import com.epam.product.mailRu.account.service.MailLoginService;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class MailLoginTest {

    private MailLoginService mailLoginService =  new MailLoginService();
    private Account account = AccountFactory.getExcistanceAccount();

    @Test
    public void correctLogin() {
        mailLoginService.login(account);
        mailLoginService.checkLoginSuccessful(account);
    }

    @Test(expectedExceptions = AuthenticationException.class
            ,expectedExceptionsMessageRegExp = "Incorrect login or password")
    public void incorrectLogin() {
        Account account = AccountFactory.getRandomAccount();
        mailLoginService.login(account);
        mailLoginService.checkErrorMessage();
    }

    @AfterMethod
    public void stopDriver() {
        Browser.getBrowser().quit();
    }
}
