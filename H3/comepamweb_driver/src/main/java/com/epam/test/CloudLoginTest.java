package com.epam.test;

import com.epam.framework.ui.Browser;
import com.epam.product.mailRu.account.businessModel.Account;
import com.epam.product.mailRu.account.businessModel.AccountFactory;
import com.epam.product.mailRu.account.service.CloudLoginService;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class CloudLoginTest {

    private CloudLoginService cloudLoginService = new CloudLoginService();

    @Test
    public void correctLogin() {
        Account account = AccountFactory.getExcistanceAccount();
        cloudLoginService.login(account);
        cloudLoginService.checkLoginSuccessful(account);
    }

    @AfterMethod
    public void stopDriver() {
        Browser.getBrowser().quit();
    }
}
