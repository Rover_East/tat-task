package com.epam.test;

import com.epam.framework.ui.Browser;
import com.epam.product.mailRu.account.businessModel.Account;
import com.epam.product.mailRu.account.businessModel.AccountFactory;
import com.epam.product.mailRu.account.service.MailLoginService;
import com.epam.product.mailRu.mail.businessModel.Letter;
import com.epam.product.mailRu.mail.businessModel.LetterBuilder;
import com.epam.product.mailRu.mail.businessModel.LetterFactory;
import com.epam.product.mailRu.mail.service.LetterService;
import org.testng.annotations.*;

public class MailTest {

    private MailLoginService mailLoginService = new MailLoginService();
    private Account account = AccountFactory.getExcistanceAccount();
    private LetterService letterService = new LetterService();

    @BeforeMethod
    public void login() {
        mailLoginService.login(account);
        mailLoginService.checkLoginSuccessful(account);
    }

    @Test
    public void writeLetterWithoutAddress() {
        Letter letter = new LetterBuilder().build();
        letterService.letterWithoutAddress(letter);
    }

    @Test
    public void checkCommonLetterInInboxAndOutcomingFolders() {
        Letter letter = LetterFactory.createLetter();
        letterService.sendFullMessageAndCheckInboxAndOutcomingFolders(letter);

    }

    @Test
    public void checkLetterWithoutTopicInInboxAndOutcomingFolders(){
        Letter letter = new LetterBuilder()
                .address("toTask@mail.ru")
                .build();
        letterService.sendMessageWithoutTopicAndCheckInboxAndOutcomingFolders(letter);
    }

    @Test()
    public void checkDraftFolder() {
        Letter letter = LetterFactory.createLetter();
        letterService.saveMessageToDraftAndCheck(letter);
    }

    @Test
    public void deleteLetterFromDraftAndTrashFolder() {
        Letter letter = LetterFactory.createLetter();
        letterService.saveMessageToDraft(letter);
        letterService.deleteLettersFromDraftAndTrashFolders(letter);
    }

    @AfterMethod
    public void stopDriver() {
        Browser.getBrowser().quit();
    }
}
