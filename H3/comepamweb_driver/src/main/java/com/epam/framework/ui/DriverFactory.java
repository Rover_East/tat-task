package com.epam.framework.ui;

import com.epam.framework.listeners.DriverListener;
import com.epam.framework.runner.Arguments;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class DriverFactory {

    private static WebDriver driver;

    public static WebDriver chromeDriver()  {

        DesiredCapabilities capabilities;

        switch (Arguments.getArguments().getBrowserType()) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver",
                        "src/main/resources/chromedriver.exe");
                capabilities = DesiredCapabilities.chrome();

//                driver = new ChromeDriver();
//                driver.manage().window().maximize();
                break;
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver",
                        "src/main/resources/geckodriver.exe");
                capabilities = DesiredCapabilities.firefox();
//                driver = new FirefoxDriver();

                break;
            default:
                throw new RuntimeException("No support for: " + Arguments.getArguments().getBrowserType());
        }
        try {
            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        EventFiringWebDriver eventFiringDriver = new EventFiringWebDriver(driver);
        eventFiringDriver.register(new DriverListener());
        return eventFiringDriver;
    }

}
