package com.epam.framework.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.framework.listeners.SuiteListener;
import com.epam.framework.listeners.TestListener;
import com.epam.framework.logging.Log;
import org.apache.log4j.PropertyConfigurator;
import org.testng.ITestNGListener;
import org.testng.TestNG;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Runner {

    private static TestNG configureTestNG() {
        TestNG testNG = new TestNG();
        testNG.setListenerClasses(Collections.singletonList(TestListener.class));
        testNG.setListenerClasses(Collections.singletonList(SuiteListener.class));
//        testNG.setTestSuites(Arrays.asList("test-suites/mailRuSuite.xml"));
        testNG.setTestSuites(Arguments.getArguments().getSuites());
        return testNG;
    }

    public static void main(String[] args) {
        parseArgs(args);
//        PropertyConfigurator.configureAndWatch("log4j.properties");
        PropertyConfigurator.configureAndWatch(Arguments.getArguments().getLog4jPath());
        Log.info("Start app...");
        configureTestNG().run();
        Log.info("End app.");
    }

    private static void parseArgs(String[] args){
        Log.info("Parse args using JCommander");
        JCommander jCommander = new JCommander(Arguments.getArguments());
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            Log.error(e.getMessage(), e);
            System.exit(1);
        }
        if (Arguments.getArguments().isHelp()) {
            jCommander.usage();
            System.exit(0);
        }
    }
}
