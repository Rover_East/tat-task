package com.epam.framework.listeners;

import com.epam.framework.logging.Log;
import com.epam.framework.runner.Arguments;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.xml.XmlSuite;

public class SuiteListener implements ISuiteListener {

    public void onStart(ISuite suite) {
        Log.info("Start suite " + suite.getName());
        suite.getXmlSuite().setParallel(Arguments.getArguments().getParallelMode());
        suite.getXmlSuite().setThreadCount(Arguments.getArguments().getThreadCount());
    }

    public void onFinish(ISuite suite) {
        Log.info("Finish suite " + suite.getName());
    }
}
