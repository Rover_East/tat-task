package com.epam.framework.ui;

import com.epam.framework.logging.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public final class Browser implements WrapsDriver {

    public static final int WAIT_TIME = 10;

    private static ThreadLocal<Browser> instance = new ThreadLocal<>();

    private WebDriver driver;

    private Browser() {
        this.driver = DriverFactory.chromeDriver();
    }

    public static synchronized Browser getBrowser() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public void quit() {
        try {
            if (getWrappedDriver() != null) {
                getWrappedDriver().quit();
            }
        } finally {
            instance.set(null);
        }
    }

    @Override
    public WebDriver getWrappedDriver() {
        return driver;
    }

    public void get(String url) {
        driver.get(url);
    }

    public WebElement findElement(By by) {
        return driver.findElement(by);
    }

    public boolean isElementPresent(By by) {
        return driver.findElements(by).size() > 0;
    }

    public boolean isElementVisible(By by) {
        if (!isElementPresent(by)) {
            return false;
        }
        return findElement(by).isDisplayed();
    }

    public void waitElementClickable(By by) {
        Log.debug("WebDriver is waiting for element clickable:" + by);
        WebElement element = new WebDriverWait(driver, WAIT_TIME).until(ExpectedConditions.elementToBeClickable(by));
        elementHighlighting(element);
    }

    public void waitElementVisible(By by) {
        Log.debug("WebDriver is waiting for element visible:" + by);
        WebElement element = new WebDriverWait(driver, WAIT_TIME).until(ExpectedConditions.visibilityOfElementLocated(by));
        elementHighlighting(element);
    }

    public void waitElementList(By by) {
        Log.debug("WebDriver is waiting for element list:" + by);
        new WebDriverWait(driver, WAIT_TIME).until(
                webDriver -> webDriver.findElements(by).size() > 0
        );
    }

    private void elementHighlighting(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].style.borderWidth = \"5px\";" +
                "arguments[0].style.borderColor = \"red\"", element);
    }

    public void acceptAlert() {
        driver.switchTo().alert().accept();
    }

    public String getAlertText() {
        Log.debug("Alert text " + driver.switchTo().alert().getText());
        return driver.switchTo().alert().getText();
    }

    public String getHandle() {
        Log.debug("Page handle " + getWrappedDriver().getWindowHandle());
        return getWrappedDriver().getWindowHandle();
    }

    public String getTitle() {
        Log.debug("Page title " + getWrappedDriver().getTitle());
        return getWrappedDriver().getTitle();
    }

    public void switchWindow(String window) {
        Log.debug("WebDriver switch to window " + window);
        getWrappedDriver().switchTo().window(window);
    }

    public Set<String> getHandles() {
        return getWrappedDriver().getWindowHandles();
    }

    public List<WebElement> findElements(By by) {
        return getWrappedDriver().findElements(by);
    }

    public File screenshot() {
        File fileToReport;
        try {
            File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            fileToReport = new File("logs/screenshots/" + System.nanoTime() + ".png").getAbsoluteFile();
            Log.info("Screenshot taken: file://" + screenshotFile.getAbsolutePath());
            FileUtils.copyFile(screenshotFile, fileToReport);
        } catch (IOException e) {
            throw new RuntimeException("Failed to write screenshot: ", e);
        }
        return fileToReport;
    }
}