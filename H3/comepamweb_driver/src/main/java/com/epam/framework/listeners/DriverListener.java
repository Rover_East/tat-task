package com.epam.framework.listeners;

import com.epam.framework.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

import java.util.Arrays;

public class DriverListener extends AbstractWebDriverEventListener {

    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
        Log.debug("WebDriver navigated to URL:" + url);
    }

    @Override
    public void afterClickOn(WebElement element, WebDriver driver) {
        Log.debug("WebDriver clicked on element: " + element);
    }

    @Override
    public void beforeClickOn(WebElement element, WebDriver driver) {
        Log.debug("Before click on " + element);
    }

    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        Log.debug("WebDriver is looking for an element:" + element);
    }

    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        Log.debug("WebDriver found an element:" + element);
    }

    @Override
    public void onException(Throwable throwable, WebDriver driver) {
        Log.error("WebDriver throw an exception", throwable);
    }

    @Override
    public void afterScript(String script, WebDriver driver) {
        Log.debug("WebDriver executed script\n" + script);
    }

    @Override
    public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
        Log.debug(String.format("WebDriver send keys %s to element %s", Arrays.toString(keysToSend), element));
    }

    @Override
    public void beforeNavigateTo(String url, WebDriver driver) {
        Log.debug("WebDriver will load new page");
    }

    @Override
    public void afterAlertAccept(WebDriver driver) {
        Log.debug("WebDriver accepted alert");
    }

    @Override
    public void afterAlertDismiss(WebDriver driver) {
        Log.debug("WebDriver dismissed alert");
    }


}
