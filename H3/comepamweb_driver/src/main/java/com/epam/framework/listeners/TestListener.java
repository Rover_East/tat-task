package com.epam.framework.listeners;

import com.epam.framework.logging.Log;
import com.epam.framework.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;

import java.io.File;

public class TestListener extends TestListenerAdapter {

    @Override
    public void onStart(ITestContext testContext) {
        Log.info("[TEST STARTED] " + testContext.getName());
    }

    @Override
    public void onFinish(ITestContext testContext) {
        Log.info("[TEST FINISHED] " + testContext.getName());
    }

    @Override
    public void onTestFailure(ITestResult tr) {
        Log.error("Test failed", tr.getThrowable());
        File file = Browser.getBrowser().screenshot();
        String filePath = file.getAbsolutePath();
        Reporter.log(String.format("<a href=\"%s\" target=”blank”><img src=\"%s\" height='250' width='250'/></a>", file, file));
        Log.info(String.format("<a href=\"%s\" target=”blank”><img src=\"%s\" height='250' width='250'/></a>", file.getAbsolutePath(), file.getAbsolutePath()));
//        Log.info(String.format("<img src=\"file://" + filePath + "\" height='100' width='100'/>", file.getAbsolutePath()));

    }
}
