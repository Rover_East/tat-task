package com.epam.framework.ui.element;

import com.epam.framework.ui.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class Element {

    private By by;

    public Element(By by) {
        this.by = by;
    }

    public By getBy() {
        return by;
    }

    public static By getFormatXpath(String xpath, String ... args){
        return By.xpath(String.format(xpath,args));
    }


    public WebElement getWrappedWebElement() {
        return Browser.getBrowser().findElement(by);
    }

    public boolean isElementVisible() {
        return Browser.getBrowser().isElementVisible(by);
    }

    public String getText() {
        waitForAppear();
        return getWrappedWebElement().getText();
    }

    public void click() {
        waitClickable();
        getWrappedWebElement().click();
    }

    public void waitForAppear(){
        Browser.getBrowser().waitElementVisible(by);
    }

    public void waitClickable(){
        Browser.getBrowser().waitElementClickable(by);
    }

    public void insertText(String value) {
        waitForAppear();
        WebElement element = getWrappedWebElement();
        element.clear();
        element.sendKeys(value);
    }

    public void sendKeys(Keys keys){
        getWrappedWebElement().sendKeys(keys);
    }

    public void switchFrame(){
        Browser.getBrowser().getWrappedDriver().switchTo().frame(getWrappedWebElement());
    }

    public void switchToDefault(){
        Browser.getBrowser().getWrappedDriver().switchTo().defaultContent();
    }

}
