package com.epam.framework.ui;

public enum BrowserType {
    CHROME, FIREFOX;
}
