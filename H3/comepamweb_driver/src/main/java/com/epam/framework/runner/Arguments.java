package com.epam.framework.runner;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.DefaultListConverter;
import com.beust.jcommander.converters.EnumConverter;
import com.epam.framework.ui.BrowserType;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class Arguments {

    private static Arguments instance;

    @Parameter(names = {"--browser","-b"},description = "browser type")
    private BrowserType browserType = BrowserType.CHROME;

    @Parameter(names = "--help", help = true, description = "How to use")
    private boolean help;

    @Parameter(names = {"--log4j"}, description = "path to log4j properties")
    private String log4jPath = "log4j.properties";

    @Parameter(names = {"--suites","-s"}, description = "path to test suites", required = true)
    private List<String> suites = new ArrayList<>();

    @Parameter(names = {"--parallelMode"}, description = "parallel mode for test suites")
    private XmlSuite.ParallelMode parallelMode = XmlSuite.ParallelMode.METHODS;

    @Parameter(names = {"--threadCount"}, description = "count of available threads")
    private int threadCount = 1;

    public static synchronized Arguments getArguments(){
        if (instance == null){
            instance = new Arguments();
        }
        return instance;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public boolean isHelp() {
        return help;
    }

    public String getLog4jPath() {
        return log4jPath;
    }

    public List<String> getSuites() {
        return suites;
    }

    public XmlSuite.ParallelMode getParallelMode() {
        return parallelMode;
    }

    public int getThreadCount() {
        return threadCount;
    }
}

