package com.epam.framework.logging;

import org.apache.log4j.Logger;

public class Log {

    private static final Logger LOG = Logger.getLogger(Log.class);

    public static void fatal(Object message,Throwable t) {
        LOG.fatal(message,t);
    }

    public static void error(Object message, Throwable t) {
        LOG.error(message, t);
    }

    public static void warn(Object message) {
        LOG.warn(message);
    }

    public static void info(Object message) {
        LOG.info(message);
    }

    public static void debug(Object message) {
        LOG.debug(message);
    }
}
