package com.epam.product.mailRu.account.service;

import com.epam.framework.logging.Log;
import com.epam.product.mailRu.account.businessModel.Account;
import com.epam.product.mailRu.account.exception.AuthenticationException;
import com.epam.product.mailRu.account.page.MailLoginPage;
import com.epam.product.mailRu.mail.page.MainPage;

public class MailLoginService {

    public void login(Account account) {
        Log.info("user " + account + " log in");
        MailLoginPage mailLoginPage = new MailLoginPage();
        mailLoginPage
                .open()
                .inputEmail(account.getEmail())
                .inputPassword(account.getPassword())
                .logIn();
    }


    public void checkErrorMessage() {
        if (isErrorPresent()) {
            Log.info("Incorrect login or password");
            throw new AuthenticationException("Incorrect login or password");
        }
    }

    public boolean isErrorPresent() {
        return new MailLoginPage().isErrorMessagePresent();
    }

    public void checkLoginSuccessful(Account account) {
        Log.info("user " + account + " logged in successful");
        MainPage mainPage = new MainPage();
        if (!mainPage.getAddress().equals(account.getEmail())) {
            throw new AuthenticationException("Auth error");
        }
    }

}
