package com.epam.product.mailRu.cloud.page;

import com.epam.framework.ui.Browser;
import com.epam.framework.ui.element.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.Set;

public class ContextMenuPage {

    private static final By SHARE_BUTTON_LOCATOR = By.xpath("//a[@data-name='publish']");
    private static final By LOOK_BUTTON_LOCATOR = By.xpath("//button[@data-name='popup']");
    private static final By FILE_NAME_LOCATOR = By.xpath("//a[@data-name='link-action']");

    private Element shareButton = new Element(SHARE_BUTTON_LOCATOR);
    private Element lookButton = new Element(LOOK_BUTTON_LOCATOR);
    private Element fileName = new Element(FILE_NAME_LOCATOR);


    public String getHandle(){
        return Browser.getBrowser().getHandle();
    }

    public void openShareMenu(){
        shareButton.click();
    }

    public void closeShareMenu(){
        lookButton.sendKeys(Keys.ESCAPE);
    }

    public void lookShareFile(){
        lookButton.click();
    }

    public String getFileName(){
        return fileName.getText();
    }

    public Set<String> getHandles(){
        return Browser.getBrowser().getHandles();
    }

    public void switchWindow(String handle){
        Browser.getBrowser().switchWindow(handle);
    }

    public String getPageTitle(){
       return Browser.getBrowser().getTitle();
    }
}
