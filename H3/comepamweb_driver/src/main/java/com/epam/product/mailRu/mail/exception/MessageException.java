package com.epam.product.mailRu.mail.exception;

public class MessageException extends RuntimeException {

    public MessageException(String message) {
        super(message);
    }
}
