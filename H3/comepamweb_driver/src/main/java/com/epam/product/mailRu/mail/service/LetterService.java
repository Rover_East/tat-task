package com.epam.product.mailRu.mail.service;

import com.epam.framework.logging.Log;
import com.epam.product.mailRu.mail.businessModel.Letter;
import com.epam.product.mailRu.mail.exception.MessageException;
import com.epam.product.mailRu.mail.page.ActionButtonsPage;
import com.epam.product.mailRu.mail.page.MailPage;
import com.epam.product.mailRu.mail.page.MainPage;
import com.epam.product.mailRu.mail.page.WriteLetterPage;
import org.openqa.selenium.TimeoutException;

import java.util.Objects;

public class LetterService {

    private WriteLetterPage writeLetterPage;
    private ActionButtonsPage actionButtonsPage = new ActionButtonsPage();


    public void writeLetter(Letter letter) {
        Log.info("Write letter " + letter);
        writeLetterPage = new MainPage().openWriteLetter();
        writeLetterPage.inputAddress(letter.getAddress())
                .inputTopic(letter.getTopic())
                .inputBody(letter.getBody())
                .send();
    }

    public void writeFullMessage(Letter letter) {
        writeLetter(letter);
        writeLetterPage.waitLetterSendMessagePresent();
    }

    public void letterWithoutTopicAndBody(Letter letter) {
        writeLetter(letter);
        writeLetterPage.closeDialogWindow();
    }

    public void letterWithoutAddress(Letter letter) {
        writeLetter(letter);
        if (writeLetterPage.getAlertText().length() == 0) {
            throw new MessageException("Alert did not present");
        }
        writeLetterPage.acceptAlert();
        writeLetterPage.cancel();
        writeLetterPage.acceptAlert();
    }

    public boolean findLetter(Letter letter) {
        Log.info("Find letter" + letter);
        MailPage mailPage = new MailPage();
        String topic = !Objects.equals(letter.getTopic(), "") ? letter.getTopic() : "<Без темы>";
        return mailPage.findLetter(topic);
    }

    public void findInInboxFolder(Letter letter) {
        Log.info("Open inbox folder");
        actionButtonsPage.goToInboxFolder();
        if (!findLetter(letter)) {
            throw new MessageException("Message " + letter + " not found in inbox folder");
        }
    }

    public void findInOutcomingFolder(Letter letter) {
        Log.info("Open outcoming folder");
        actionButtonsPage.goToOutcomingFolder();
        if (!findLetter(letter)) {
            throw new MessageException("Message " + letter + " not found in outcoming folder");
        }
    }

    public void findInDraftFolder(Letter letter) {
        Log.info("Open draft folder");
        actionButtonsPage.goToDraftFolder();
        if (!findLetter(letter)) {
            throw new MessageException("Message " + letter + " not found in draft folder");
        }
    }

    public void sendFullMessageAndCheckInboxAndOutcomingFolders(Letter letter) {
        writeFullMessage(letter);
        findInOutcomingFolder(letter);
        findInInboxFolder(letter);
    }

    public void sendMessageWithoutTopicAndCheckInboxAndOutcomingFolders(Letter letter) {
        letterWithoutTopicAndBody(letter);
        findInOutcomingFolder(letter);
        findInInboxFolder(letter);
    }

    public void saveMessageToDraft(Letter letter) {
        writeLetterPage = new MainPage().openWriteLetter();
        writeLetterPage.inputAddress(letter.getAddress())
                .inputTopic(letter.getTopic())
                .inputBody(letter.getBody())
                .save();
        writeLetterPage.waitLetterSavedMessagePresent();
    }

    public void saveMessageToDraftAndCheck(Letter letter) {
        saveMessageToDraft(letter);
        findInDraftFolder(letter);
    }

    public void deleteLetters(Letter letter) {
        MailPage mailPage = new MailPage();
        String topic = !Objects.equals(letter.getTopic(), "") ? letter.getTopic() : "<Без темы>";
        try {
            mailPage.selectLetters(topic);
            mailPage.deleteLetter();
            mailPage.waitLetterDeletedMessagePresent();
            mailPage.pageUp();
            findLetter(letter);
        } catch (TimeoutException e) {
            return;
        }
    }

    public void deleteLettersFromDraftAndTrashFolders(Letter letter) {
        Log.info("Open draft folder");
        actionButtonsPage.goToDraftFolder();
        deleteLetters(letter);
        Log.info("Open trash folder");
        actionButtonsPage.goToTrashFolder();
        deleteLetters(letter);
    }

}
