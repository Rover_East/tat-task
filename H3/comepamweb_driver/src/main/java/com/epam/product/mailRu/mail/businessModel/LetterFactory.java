package com.epam.product.mailRu.mail.businessModel;

import com.epam.framework.util.Randoms;

public class LetterFactory {

    public static Letter createLetter(){
        Letter letter = new Letter();
        letter.setAddress("toTask@mail.ru");
        letter.setTopic(Randoms.randomAlphanumeric());
        letter.setBody(Randoms.randomAlphanumeric());
        return letter;
    }
}
