package com.epam.product.mailRu.cloud.page;

import com.epam.framework.ui.element.Element;
import org.openqa.selenium.By;

public class CloudPage {

    private static final By EMAIL_LOCATOR = By.id("PH_user-email");

    private Element email = new Element(EMAIL_LOCATOR);

    public String getAddress(){
        return email.getText();
    }

}
