package com.epam.product.mailRu.mail.businessModel;

public class LetterBuilder {

    private Letter letter = new Letter();

    public LetterBuilder address(String address){
        letter.setAddress(address);
        return this;
    }

    public LetterBuilder topic (String topic){
        letter.setTopic(topic);
        return this;
    }

    public LetterBuilder body(String body){
        letter.setBody(body);
        return this;
    }

    public Letter build(){
        return letter;
    }
}
