package com.epam.product.mailRu.mail.page;

import com.epam.framework.ui.Browser;
import com.epam.framework.ui.element.Element;
import org.openqa.selenium.*;

import java.util.List;

public class MailPage {

    private static final String MAIL_CHECKBOX = "//div[not(contains(@style,'display: none')) and @data-cache-key]//a[@data-subject='%s']//div[@class='b-checkbox__box']";
    private static final String MAIL = "//div[not(contains(@style,'display: none')) and @data-cache-key]//div[contains(text(),'%s')]";

    private static final By HTML_TAG_LOCATOR = By.tagName("html");
    private static final By DELETE_BUTTON_LOCATOR = By.xpath("//div[not(contains(@style,'display: none')) and @data-cache-key]//div[@data-shortcut-title='Del']/span");
    private static final By LETTER_DELETED_MESSAGE_LOCATOR = By.cssSelector("body > div.notify > div.js-ok.notify-message span");

    private Element deleteButton = new Element(DELETE_BUTTON_LOCATOR);
    private Element letterDeletedMessage = new Element(LETTER_DELETED_MESSAGE_LOCATOR);

    public boolean findLetter(String topic) {
        Element letter = new Element(Element.getFormatXpath(MAIL, topic));
        letter.waitClickable();
        return letter.isElementVisible();
    }

    public void deleteLetter() {
        deleteButton.click();
    }

    public void waitLetterDeletedMessagePresent() {
        letterDeletedMessage.waitForAppear();
    }

    public void pageUp() {
        new Element(HTML_TAG_LOCATOR).sendKeys(Keys.PAGE_UP);
    }

    public void selectLetters(String topic) throws TimeoutException{

        List<WebElement> webElements;
        Browser.getBrowser().waitElementClickable(Element.getFormatXpath(String.format(MAIL_CHECKBOX,topic)));
        webElements = Browser.getBrowser()
                .findElements(Element.getFormatXpath(String.format(MAIL_CHECKBOX,topic)));
        webElements.forEach(WebElement::click);
    }

}
