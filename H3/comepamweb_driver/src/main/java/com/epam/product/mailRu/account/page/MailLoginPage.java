package com.epam.product.mailRu.account.page;

import com.epam.framework.ui.Browser;
import com.epam.framework.ui.element.Element;
import org.openqa.selenium.By;

public class MailLoginPage {

    private static final String URL = "https://mail.ru/";

    private static final By LOGIN_INPUT_LOCATOR = By.id("mailbox:login");
    private static final By PASSWORD_INPUT_LOCATOR = By.id("mailbox:password");
    private static final By SUBMIT_BUTTON_LOCATOR = By.id("mailbox:submit");
    private static final By ERROR_MESSAGE_LOCATOR = By.id("mailbox:error");

    private Element loginInput = new Element(LOGIN_INPUT_LOCATOR);
    private Element passwordInput = new Element(PASSWORD_INPUT_LOCATOR);
    private Element submitButton = new Element(SUBMIT_BUTTON_LOCATOR);
    private Element errorMessage = new Element(ERROR_MESSAGE_LOCATOR);

    public MailLoginPage open(){
        Browser.getBrowser().get(URL);
        return this;
    }

    public MailLoginPage inputEmail(String email){
        loginInput.insertText(email);
        return this;
    }

    public MailLoginPage inputPassword(String password){
        passwordInput.insertText(password);
        return this;
    }

    public void logIn(){
        submitButton.click();
    }

    public boolean isErrorMessagePresent() {
        errorMessage.waitForAppear();
        return errorMessage.isElementVisible();
    }
}
