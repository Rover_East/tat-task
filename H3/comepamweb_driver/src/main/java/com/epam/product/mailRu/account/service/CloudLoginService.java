package com.epam.product.mailRu.account.service;

import com.epam.framework.logging.Log;
import com.epam.product.mailRu.account.businessModel.Account;
import com.epam.product.mailRu.account.exception.AuthenticationException;
import com.epam.product.mailRu.account.page.CloudLoginPage;
import com.epam.product.mailRu.cloud.page.CloudPage;

import java.util.Objects;

public class CloudLoginService {

    public void login(Account account) {
        Log.info("user "+account+" log in");
        CloudLoginPage cloudLoginPage = new CloudLoginPage();
        cloudLoginPage
                .open()
                .openLoginForm()
                .inputEmail(account.getEmail())
                .inputPassword(account.getPassword())
                .logIn();

    }

    public void checkLoginSuccessful(Account account){
        CloudPage cloudPage = new CloudPage();
        if (!Objects.equals(cloudPage.getAddress(), account.getEmail())){
            throw new AuthenticationException("Auth error");
        }
    }
}
