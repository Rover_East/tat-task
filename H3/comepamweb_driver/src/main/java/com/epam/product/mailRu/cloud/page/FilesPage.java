package com.epam.product.mailRu.cloud.page;

import com.epam.framework.ui.Browser;
import com.epam.framework.ui.element.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

public class FilesPage {

    private static final String FILE_CHECKBOX_LOCATOR = "//div[@data-id = '/%s' and @data-name='link']//div[@class ='b-checkbox__box']";
    private static final String FILE_LOCATOR = "//div[@data-id = '/%s' and @data-name='link']";
    private static final String FILE_IN_FOLDER_LOCATOR = "//div[contains(@data-id ,'%s') and @data-name='link']";

    private static final By REMOVE_BUTTON_LOCATOR = By.xpath("//div[@data-name='remove']");
    private static final By CONFIRM_REMOVE_LOCATOR = By.xpath("//button[@data-name='remove']");
    private static final By FILE_DELETED_MESSAGE_LOCATOR = By.cssSelector("span.notify-message__title__text_ok");
    private static final By OK_MESSAGE_LOCATOR = By.xpath("//span[text()='Спасибо, понятно']");
    private static final By MOVE_LOCATOR = By.xpath("//button[@data-name='move']");
    private static final By ROOT_FOLDER_LOCATOR = By.xpath("//a[@data-name='Облако']");

    private Element removeButton = new Element(REMOVE_BUTTON_LOCATOR);
    private Element confirmRemove = new Element(CONFIRM_REMOVE_LOCATOR);
    private Element fileDeletedMessage = new Element(FILE_DELETED_MESSAGE_LOCATOR);
    private Element okMessage = new Element(OK_MESSAGE_LOCATOR);
    private Element move = new Element(MOVE_LOCATOR);
    private Element rootFolder = new Element(ROOT_FOLDER_LOCATOR);

    public FilesPage selectFile(String name) {
        Element fileCheckbox = new Element(Element.getFormatXpath(FILE_CHECKBOX_LOCATOR, name));
        fileCheckbox.click();
        return this;
    }

    public FilesPage remove() {
        removeButton.click();
        return this;
    }

    public FilesPage confirmRemove() {
        confirmRemove.click();
        return this;
    }

    public void getFileDeletedMessage() {
        fileDeletedMessage.waitForAppear();
    }

    public FilesPage closeOkMessage() {
        okMessage.click();
        return this;
    }

    public FilesPage confirmMove() {
        move.click();
        return this;
    }

    public void goToRootFolder() {
        rootFolder.click();
    }

    public void waitFile(String name) {
        new Element(Element.getFormatXpath(FILE_LOCATOR, name)).waitClickable();
    }

    public boolean findFile(String name) {
        return new Element(Element.getFormatXpath(FILE_LOCATOR, name)).isElementVisible();
    }

    public boolean findFileInFolder(String name) {
        Element file = new Element(Element.getFormatXpath(FILE_IN_FOLDER_LOCATOR, name));
        file.waitClickable();
        return file.isElementVisible();
    }

    public FilesPage dragAndDropObject(String fileName, String folderName) {
        Element fileToMove = new Element(Element.getFormatXpath(FILE_LOCATOR, fileName));
        Element folder = new Element(Element.getFormatXpath(FILE_LOCATOR, folderName));

        new Actions(Browser.getBrowser().getWrappedDriver())
                .dragAndDrop(fileToMove.getWrappedWebElement(), folder.getWrappedWebElement())
                .build()
                .perform();
        return this;
    }

    public FilesPage openFolder(String folderName) {
        waitFile(folderName);
        new Element(Element.getFormatXpath(FILE_LOCATOR, folderName)).click();
        return this;
    }

    public ContextMenuPage openContextMenu(String name) {
        Element file = new Element(Element.getFormatXpath(FILE_CHECKBOX_LOCATOR, name));
        new Actions(Browser.getBrowser().getWrappedDriver())
                .contextClick(file.getWrappedWebElement())
                .build()
                .perform();
        return new ContextMenuPage();
    }
}
