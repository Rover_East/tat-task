package com.epam.product.mailRu.mail.page;

import com.epam.framework.ui.element.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ActionButtonsPage {

    private static final By TO_SENT_LOCATOR = By.cssSelector("div#b-nav_folders a.b-nav__link[href*='inbox']");
    private static final By TO_OUTCOMING_LOCATOR = By.cssSelector("div#b-nav_folders a.b-nav__link[href*='sent']");
    private static final By TO_DRAFT_LOCATOR = By.cssSelector("div#b-nav_folders a.b-nav__link[href*='drafts']");
    private static final By TO_TRASH_LOCATOR = By.cssSelector("div#b-nav_folders a.b-nav__link[href*='trash']");

    private Element sentFolderButton = new Element(TO_SENT_LOCATOR);
    private Element outcomingFolderButton = new Element(TO_OUTCOMING_LOCATOR);
    private Element draftFolderButton = new Element(TO_DRAFT_LOCATOR);
    private Element trashFolderButton = new Element(TO_TRASH_LOCATOR);


    public MailPage goToInboxFolder() {
        sentFolderButton.click();
        return new MailPage();
    }

    public MailPage goToOutcomingFolder() {
        outcomingFolderButton.click();
        return new MailPage();
    }

    public MailPage goToDraftFolder() {
        draftFolderButton.click();
        return new MailPage();
    }
    public MailPage goToTrashFolder() {
        trashFolderButton.click();
        return new MailPage();
    }
}
