package com.epam.product.mailRu.cloud.exception;

public class CloudException extends RuntimeException {

    public CloudException(String message) {
        super(message);
    }
}
