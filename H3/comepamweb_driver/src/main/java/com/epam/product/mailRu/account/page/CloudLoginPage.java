package com.epam.product.mailRu.account.page;

import com.epam.framework.ui.Browser;
import com.epam.framework.ui.element.Element;
import org.openqa.selenium.By;

public class CloudLoginPage  {

    private static final String URL = "https://cloud.mail.ru/";

    private static final By AUTH_LINK_LOCATOR = By.id("PH_authLink");
    private static final By LOGIN_INPUT_LOCATOR = By.id("ph_login");
    private static final By PASSWORD_INPUT_LOCATOR = By.id("ph_password");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//span[@data-action='login']");

    private Element authLink = new Element(AUTH_LINK_LOCATOR);
    private Element loginInput = new Element(LOGIN_INPUT_LOCATOR);
    private Element passwordInput = new Element(PASSWORD_INPUT_LOCATOR);
    private Element loginButton = new Element(LOGIN_BUTTON_LOCATOR);

    public CloudLoginPage open(){
        Browser.getBrowser().get(URL);
        return this;
    }

    public CloudLoginPage openLoginForm(){
        authLink.click();
        return this;
    }

    public CloudLoginPage inputEmail(String email){
        loginInput.insertText(email);
        return this;
    }

    public CloudLoginPage inputPassword(String password){
        passwordInput.insertText(password);
        return this;
    }

    public void logIn(){
        loginButton.click();
    }
}
