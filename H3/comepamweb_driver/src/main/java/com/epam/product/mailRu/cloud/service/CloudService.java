package com.epam.product.mailRu.cloud.service;

import com.epam.framework.logging.Log;
import com.epam.product.mailRu.cloud.exception.CloudException;
import com.epam.product.mailRu.cloud.page.ActionButtonsPage;
import com.epam.product.mailRu.cloud.page.ContextMenuPage;
import com.epam.product.mailRu.cloud.page.FilesPage;

import java.io.File;

public class CloudService {

    public void findFile(String name) {
        Log.info("Find letter " + name);
        FilesPage filesPage = new FilesPage();
        if (!filesPage.findFile(name)) {
            throw new CloudException("file did not found " + name);
        }
    }

    public void createFolder(String name) {
        Log.info("Create folder " + name);
        ActionButtonsPage actionButtonsPage = new ActionButtonsPage();
        actionButtonsPage
                .openCreateMenu()
                .createFolder()
                .inputFolderName(name)
                .completeFolderCreation();
        new FilesPage().waitFile(name);
    }

    public void deleteFolder(String name) {
        Log.info("Delete folder " + name);
        FilesPage filesPage = new FilesPage();
        filesPage
                .selectFile(name)
                .remove()
                .confirmRemove()
                .closeOkMessage()
                .getFileDeletedMessage();
    }

    public void findCreatedFolder(String name) {
        createFolder(name);
        findFile(name);
    }

    public void findRemovedFolder(String name) {
        createFolder(name);
        deleteFolder(name);
        findFile(name);
    }

    public void uploadFile(String name) {
        Log.info("Upload file " + name);
        ActionButtonsPage actionButtonsPage = new ActionButtonsPage();
        actionButtonsPage.openUploadMenu().uploadFile(new File("src/main/resources/" + name).getAbsolutePath());
        new FilesPage().waitFile(name);
        findFile(name);
    }

    public void moveFromTo(String from, String to) {
        Log.info(String.format("Move file %s to %s", from, to));
        FilesPage filesPage = new FilesPage();
        createFolder(to);
        uploadFile(from);
        filesPage
                .dragAndDropObject(from, to)
                .confirmMove()
                .openFolder(to);
        if (!filesPage.findFileInFolder(from)) {
            throw new CloudException(String.format("file %s not found in folder %s", from, to));
        }
        filesPage.goToRootFolder();
        deleteFolder(to);
    }

    private void switchWindow(String title) {
        Log.info("Switch to window with title " + title);
        ContextMenuPage contextMenuPage = new ContextMenuPage();
        for (String handle : contextMenuPage.getHandles()) {
            contextMenuPage.switchWindow(handle);
            if (title.contains(contextMenuPage.getPageTitle())) {
                break;
            }
        }
    }

    public void shareFile(String name) {
        Log.info("Share file " + name);
        FilesPage filesPage = new FilesPage();
        ContextMenuPage contextMenuPage = new ContextMenuPage();
        filesPage.openContextMenu(name);
        String baseHandle = contextMenuPage.getHandle();
        contextMenuPage.openShareMenu();
        contextMenuPage.lookShareFile();
        switchWindow(name);
        if (!contextMenuPage.getFileName().equals(name)) {
            throw new CloudException("File name not match");
        }
        contextMenuPage.switchWindow(baseHandle);
        contextMenuPage.closeShareMenu();
    }

    public void uploadAndShareFile(String name) {
        uploadFile(name);
        shareFile(name);
    }
}
