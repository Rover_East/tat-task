package com.epam.product.mailRu.cloud.page;

import com.epam.framework.ui.element.Element;
import org.openqa.selenium.By;

public class ActionButtonsPage {


    private static final By CREATE_BUTTON_LOCATOR = By.xpath("//div[@data-group='create']");
    private static final By FOLDER_BUTTON_LOCATOR = By.xpath("//a[@data-name='folder']");
    private static final By FOLDER_NAME_INPUT_LOCATOR = By.cssSelector(".layer__input");
    private static final By ADD_FOLDER_BUTTON_LOCATOR = By.xpath("//button[@data-name='add']");

    private static final By UPLOAD_BUTTON_LOCATOR = By.xpath("//div[@data-name='upload']");
    private static final By CHOOSE_FILES_LOCATOR = By.xpath("//input[@class='layer_upload__controls__input']");

    private Element createButton = new Element(CREATE_BUTTON_LOCATOR);
    private Element folderButton = new Element(FOLDER_BUTTON_LOCATOR);
    private Element folderNameInput = new Element(FOLDER_NAME_INPUT_LOCATOR);
    private Element addFolderButton = new Element(ADD_FOLDER_BUTTON_LOCATOR);
    private Element uploadButton = new Element(UPLOAD_BUTTON_LOCATOR);
    private Element chooseFiles = new Element(CHOOSE_FILES_LOCATOR);

    public ActionButtonsPage openCreateMenu(){
        createButton.click();
        return this;
    }

    public ActionButtonsPage createFolder(){
        folderButton.click();
        return this;
    }

    public ActionButtonsPage inputFolderName(String name){
        folderNameInput.insertText(name);
        return this;
    }

    public void completeFolderCreation(){
        addFolderButton.click();
    }

    public ActionButtonsPage openUploadMenu(){
        uploadButton.click();
        return this;
    }

    public void uploadFile(String filePath){
        chooseFiles.getWrappedWebElement().sendKeys(filePath);
    }
}
