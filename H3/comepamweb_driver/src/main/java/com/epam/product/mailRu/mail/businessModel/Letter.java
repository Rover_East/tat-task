package com.epam.product.mailRu.mail.businessModel;

public class Letter {

    private String address = "";
    private String topic = "";
    private String body = "";

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Letter{" +
                "address='" + address + '\'' +
                ", topic='" + topic + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
