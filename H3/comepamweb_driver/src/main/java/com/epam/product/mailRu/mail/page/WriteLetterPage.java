package com.epam.product.mailRu.mail.page;

import com.epam.framework.ui.Browser;
import com.epam.framework.ui.element.Element;
import org.openqa.selenium.By;

public class WriteLetterPage {

    private static final By CONTINUE_BUTTON_LOCATOR = By.cssSelector(".is-compose-empty_in button.confirm-ok");
    private static final By CANCEL_BUTTON_LOCATOR = By.xpath("//div[@data-name ='cancel']");
    private static final By LETTER_BODY_LOCATOR = By.id("tinymce");
    private static final By LETTER_SAVED_SUCCESSFUL_LOCATOR = By.xpath("//div[contains(text(),'Сохранено в ')]");
    private static final By LETTER_SEND_SUCCESSFUL_LOCATOR = By.cssSelector("div.message-sent__title");
    private static final By ADDRESS_LOCATOR = By.cssSelector("textarea.js-input.compose__labels__input[data-original-name='To']");
    private static final By TOPIC_LOCATOR = By.cssSelector("input.b-input[name='Subject']");
    private static final By FRAME_LOCATOR = By.cssSelector("tbody iframe");
    private static final By SAVE_BUTTON_LOCATOR = By.xpath("//div[@data-shortcut-title='Ctrl+S']");
    private static final By SEND_BUTTON_LOCATOR = By.xpath("//div[@data-shortcut-title='Ctrl+Enter']");


    private Element continueButton = new Element(CONTINUE_BUTTON_LOCATOR);
    private Element cancelButton = new Element(CANCEL_BUTTON_LOCATOR);
    private Element body = new Element(LETTER_BODY_LOCATOR);
    private Element messageSavedMessage = new Element(LETTER_SAVED_SUCCESSFUL_LOCATOR);
    private Element messageSendMessage = new Element(LETTER_SEND_SUCCESSFUL_LOCATOR);

    private Element address = new Element(ADDRESS_LOCATOR);
    private Element topic = new Element(TOPIC_LOCATOR);
    private Element frame = new Element(FRAME_LOCATOR);
    private Element saveButton = new Element(SAVE_BUTTON_LOCATOR);
    private Element sendButton = new Element(SEND_BUTTON_LOCATOR);


    public WriteLetterPage inputAddress(String address) {
        this.address.insertText(address);
        return this;
    }

    public WriteLetterPage inputTopic(String topic) {
        this.topic.insertText(topic);
        return this;
    }

    public WriteLetterPage inputBody(String body) {
        frame.switchFrame();
        this.body.insertText(body);
        frame.switchToDefault();
        return this;
    }

    public void acceptAlert() {
        Browser.getBrowser().acceptAlert();
    }

    public String getAlertText() {
        return Browser.getBrowser().getAlertText();
    }

    public void save() {
        saveButton.click();
    }

    public void send() {
        sendButton.click();
    }

    public void waitLetterSavedMessagePresent() {
        messageSavedMessage.waitForAppear();
    }

    public void waitLetterSendMessagePresent() {
        messageSendMessage.waitForAppear();
    }

    public void closeDialogWindow() {
        continueButton.click();
    }

    public void cancel() {//TODO нужно переименовать, тк не помню , что делает кнопка
        cancelButton.click();
    }

}
