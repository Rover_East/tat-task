package com.epam.product.mailRu.mail.page;

import com.epam.framework.ui.element.Element;
import org.openqa.selenium.By;

public class MainPage  {

    private static final By EMAIL_ID_LOCATOR = By.id("PH_user-email");
    private static final By WRITE_LETTER_BUTTON_LOCATOR = By.cssSelector("#b-toolbar__left div.b-toolbar__group.b-toolbar__group_left");

    private Element email = new Element(EMAIL_ID_LOCATOR);
    private Element writeLetterButton =  new Element(WRITE_LETTER_BUTTON_LOCATOR);


    public String getAddress() {
        return email.getText();
        }

    public WriteLetterPage openWriteLetter() {
        writeLetterButton.click();
        return new WriteLetterPage();
    }
}
