package com.epam.gomel.homework;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.epam.gomel.homework.BoyHamcrestMatcher.hasWealth;
import static com.epam.gomel.homework.BoyHamcrestMatcher.girlNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BoyHamcrestTest {

    private Boy boy ;

    @DataProvider(name = "user data")
    public Object[][] createUser(){
        return new Object[][]{
                {Month.JANUARY,100000},
                {Month.JULY,1}
        };
    }

    @Test(dataProvider = "user data")
    public void testMonth(Month month, double wealth) {
        boy = new Boy(month,wealth);
        assertThat(boy.getBirthdayMonth(),is(month));
    }

//    @Test(dataProvider = "user data",description = "test boy wealth")
//    public void testWealth(Month month, double wealth) {
//        boy = new Boy(month,wealth);
//        assertThat(boy.getWealth(),equalTo(hasWealth(wealth)));
//    }

    @Test(dataProvider = "user data", description = "test boy girlFriend")
    public void testGirlNotNull(Month month, double wealth){
        boy = new Boy(month,wealth);
        assertThat(boy,is(girlNull()));
    }


}
