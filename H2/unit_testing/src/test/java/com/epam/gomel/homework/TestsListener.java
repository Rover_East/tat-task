package com.epam.gomel.homework;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import java.time.LocalTime;

public class TestsListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult iTestResult) {
        System.out.println("[METHOD_STARTED] - "+ LocalTime.now() + " " + method.getTestMethod().getMethodName());
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.println(String.format("[METHOD_FINISHED] - %s >>> %s",
                method.getTestMethod().getMethodName(), testResult.getStatus() == 1 ? "Success" : "Fail"));
    }
}
