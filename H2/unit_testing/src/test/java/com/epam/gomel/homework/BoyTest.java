package com.epam.gomel.homework;


import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class BoyTest {

    private Boy boy;
    private Girl girl;

    @Test(groups = "mood")
    public void testExcellentMood(){
        boy = new Boy(Month.JUNE,1020000);
        girl = new Girl(true);
        boy.setGirlFriend(girl);
        Assert.assertEquals(boy.getMood(),Mood.EXCELLENT,"wrong boy mood");
    }

    @Test(groups = "mood")
    public void testGoodMood(){
        boy = new Boy(Month.NOVEMBER,1020000);
        girl = new Girl(true);
        boy.setGirlFriend(girl);
        Assert.assertEquals(boy.getMood(),Mood.GOOD,"wrong boy mood");
    }

    @Test(groups = "mood")
    public void testNeutralMood(){
        boy = new Boy(Month.JUNE,1020000);
        Assert.assertEquals(boy.getMood(),Mood.NEUTRAL,"wrong boy mood");
    }

    @Test(groups = "mood",priority = 4)
    public void testBadMood(){
        boy = new Boy(Month.JUNE);
        Assert.assertEquals(boy.getMood(),Mood.BAD,"wrong boy mood");
    }

    @Test(groups = "mood",priority = 3)
    public void testHorribleMood(){
        boy = new Boy(Month.JANUARY);
        Assert.assertEquals(boy.getMood(),Mood.HORRIBLE,"wrong boy mood");
    }

    @Test(groups = "spendMoney",
            dependsOnGroups = "negative")
    public void testSpendSomeMoney(){
        boy = new Boy(Month.JANUARY,10);
        boy.spendSomeMoney(9);
        Assert.assertEquals(boy.getWealth(),10.0-9,"wealth is wrong");
    }

    @Test(groups = {"spendMoney","negative"},
            expectedExceptions = RuntimeException.class,expectedExceptionsMessageRegExp = "Not enough money!.*")
    public void testSpendSomeMoreMoney(){
        boy = new Boy(Month.JANUARY,10);
        boy.spendSomeMoney(11);
    }
}