package com.epam.gomel.homework;

import org.testng.Assert;
import org.testng.annotations.Test;

public class GirlTest {

    private Boy boy;
    private Girl girl;

    @Test(groups = "mood")
    public void testExcellentMood(){
        boy = new Boy(Month.JUNE,1020000);
        girl = new Girl(true,true,boy);
        girl.setPretty(true);
        girl.setBoyFriend(boy);
        Assert.assertEquals(girl.getMood(),Mood.EXCELLENT, "wrong girl mood");
    }

    @Test(groups = "mood")
    public void testGoodMood(){
        girl = new Girl(true,false);
        Assert.assertEquals(girl.getMood(),Mood.GOOD, "wrong girl mood");
    }

    @Test(groups = "mood")
    public void testNeutralmood(){
        girl = new Girl();
        Assert.assertEquals(girl.getMood(),Mood.NEUTRAL, "wrong girl mood");
    }

    @Test(groups = "mood")
    public void testIHateThemAlllmood(){
        girl = new Girl(false,false);
        Assert.assertEquals(girl.getMood(),Mood.I_HATE_THEM_ALL, "wrong girl mood");
    }

    @Test(groups = "spendMoney")
    public void testSpendSomeMoney(){
        boy = new Boy(Month.JANUARY,1200000);
        girl = new Girl(true,true,boy);
        girl.spendBoyFriendMoney(9);
        Assert.assertEquals(girl.getBoyFriend().getWealth(),1200000.0-9,"wealth is wrong");
    }

    @Test(groups = {"spendMoney","negative"},
            expectedExceptions = RuntimeException.class,expectedExceptionsMessageRegExp = "Not enough money!.*")
    public void testSpendSomeMoreMoney(){
        boy = new Boy(Month.JANUARY,1200000);
        girl = new Girl(true,true,boy);
        girl.spendBoyFriendMoney(1200001);
    }

}