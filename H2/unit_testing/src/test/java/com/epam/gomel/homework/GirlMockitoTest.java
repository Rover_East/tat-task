package com.epam.gomel.homework;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GirlMockitoTest {

    private Girl girl = new Girl(true,true);

    @Test
    public void testSpendBoyFriendMoney(){
        Boy mockedBoy= mock(Boy.class);
        when(mockedBoy.isRich()).thenReturn(true);
        mockedBoy.isRich();
        girl.setBoyFriend(mockedBoy);
        verify(mockedBoy).isRich();
        Assert.assertTrue(girl.isBoyfriendRich(),"boy is not rich");
    }

    @Test
    public void testGetBoyMoney(){
        Boy mockedBoy= mock(Boy.class);
        when(mockedBoy.getWealth()).thenCallRealMethod();
        mockedBoy.getWealth();
        girl.setBoyFriend(mockedBoy);
        verify(mockedBoy).getWealth();
        Assert.assertEquals(mockedBoy.getWealth(),girl.getBoyFriend().getWealth(),"wrong boy wealth");
    }
}
