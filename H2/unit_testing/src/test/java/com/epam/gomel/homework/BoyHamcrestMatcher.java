package com.epam.gomel.homework;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import static org.hamcrest.Matchers.*;

public class BoyHamcrestMatcher {

    public static Matcher<Boy> girlNull() {
        return new FeatureMatcher<Boy, Boolean>(is(true), "girl should be not null", "not null -") {
            @Override
            protected Boolean featureValueOf(Boy boy) {
                return boy.getGirlFriend() == null;
            }
        };
    }

    public static Matcher<Boy> hasWealth(double wealth) {
        return new FeatureMatcher<Boy, Double>(equalTo(wealth), "boy should has money", "money -") {
            @Override
            protected Double featureValueOf(Boy boy) {
                return boy.getWealth();
            }
        };
    }
}
