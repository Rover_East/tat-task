package com.epam.gomel.homework;

import org.testng.TestNG;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestRunner {

    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> testFiles = new ArrayList<>();
        testFiles.add("./src/test/resources/testng.xml");
        testNG.setTestSuites(testFiles);
        testNG.run();
    }
}
